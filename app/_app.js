/**
 * @author Doğan TV Software Section
 */

var UXQuarkJS = UXQuarkJS || {};

(function(app) {
    'use strict';

    app.initialized = false;

    app.init = function() {
        if(app.initialized) {
            console.warn('%cUXQuarkJS', 'font-weight: bold', 'is allready initialized');
            return;
        }
        app.initialized = true;

        app.runPlugins();
        console.log('zort');
    };

    app.runPlugins = function() {};

})(UXQuarkJS);

$(UXQuarkJS.init);